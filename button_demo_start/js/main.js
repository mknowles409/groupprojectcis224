//jQuery Button Demo
//Name: Morgan Knowles
//Date: 2/16/2017

console.log("jQuery Button Demo Project \nName: Morgan Knowles \nDate: 3/2/2017");


$(document).ready(function(){

	var monkeyMessage = $("#message");
	var wiseDivFirst = $("#wiseMonkeyFirst");
	var wiseDivSecond = $("#wiseMonkeySecond");
	var monkeyPic;
	var clicks = 0;
	
	$("button").each(function(){
		$(this).click(function(){//listens for a click to this button

			clicks++;
			monkeyPic = $(this).find("img").attr("src");//sets monkeyPic to the img that was clicked
			var url = "url(" + monkeyPic + ")"; //creates a proper css url

			if(clicks % 2  === 0){ //if we're attempting to fill the second div
			 	wiseDivSecond.css("background-image", url); //set the background image of the wiseDiv to the image from the button that was clicked
			 	
			 	if(wiseDivFirst.css("background-image") === wiseDivSecond.css("background-image")){
					monkeyMessage.text($(this).find("img").attr("alt"));//set monkeyMessage to the alt attribute of the image of the button that was clicked
				}else{
					monkeyMessage.text("That was unwise");

			}}else{ //if we're attempting to fill the first div
				wiseDivFirst.css("background-image", url); //sets the background image of the wiseDiv to the image from the button that was clicked				
				
				if(wiseDivSecond.css("background-image") != "none"){ //if wiseDivSecond has a background-image
					wiseDivSecond.css("background-image", "none"); //resets css style for wiseDivSecond
					monkeyMessage.text(" "); //clears monkeyMessage
					console.log("")
				}				
			}	
		});
	});
});












/*
This will make js files work if we put the src at the top of the html document
window.onload = function(){
	var wise = document.getElementById("wiseMonkey");
	wise.innerHTML = "Wise Monkey says hello!";
}
*/


//$(); //The jQuery object

/*How to make js work with html. This is the same thing as window.onload. 
It requires an argument. 
We can create an anonymous function or callback - function(){}

This: 
	$(document).ready(function(){});

Is the same as this:
	$(
		function(){
			
		}
	);

And this:
	$(document).ready(
		function()
			{
				
			}
	);
*/

/*
Example of js
	var wise = document.getElementById("wiseMonkey");
	wise.innerHTML = "Wise Monkey says hello!";
Example of jQuery
	$("#wiseMonkey").html("Wise Monkey says hello!");
*/
